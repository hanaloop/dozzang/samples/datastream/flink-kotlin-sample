package com.disney.studiotech.sample.kflink

import com.disney.studiotech.sample.kflink.util.CollectionSink
import com.disney.studiotech.sample.kflink.util.CsvUtil
import org.apache.flink.api.common.typeinfo.TypeHint
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.java.ExecutionEnvironment
import org.apache.flink.api.java.tuple.Tuple3
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment
import org.apache.flink.table.api.TableEnvironment
import org.apache.flink.table.api.Types
import org.apache.flink.table.sources.CsvTableSource
import org.apache.flink.types.Row
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.nio.file.Files
import java.nio.file.Paths


class KFlinkTest {

    companion object {
        val GLOBAL_TEMPERATURE_FILE = "./src/test/resources/testdata/GlobalLandTemperatures_GlobalLandTemperaturesByCountry.csv"
    }

    @Test
    fun batch_loadCsv_and_queryAverage() {

        val testOutPath = "./test_out/fromBatch_loadCsv_and_queryAverage.csv"

        val env = ExecutionEnvironment.getExecutionEnvironment()
        env.parallelism = 1

        val tableEnv = TableEnvironment.getTableEnvironment(env)

        val csvTable = buildGlobalTempCsvTable(tableEnv)

        val result = tableEnv.sqlQuery("select Country, YEAR(dt), AVG(AverageTemperature) from source " +
                " WHERE AverageTemperature IS NOT NULL AND Country = 'United States' " +
                " GROUP BY Country, YEAR(dt)")

        val typeInfo = TypeInformation.of(object : TypeHint<Tuple3<String, Long, Double>>() {})

        if (Files.exists(Paths.get(testOutPath))) {
            Files.delete(Paths.get(testOutPath))
        }
        tableEnv.toDataSet(result, typeInfo).writeAsCsv(testOutPath)

        env.execute()

        val resultCsv = CsvUtil.readAll(testOutPath)

        assertThat(resultCsv).hasSize(214)
    }

    @Test
    fun stream_loadCsv_and_query() {

        val env = StreamExecutionEnvironment.getExecutionEnvironment()
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
        env.parallelism = 1

        val tableEnv = TableEnvironment.getTableEnvironment(env)

        val csvTable = buildGlobalTempCsvTable(tableEnv)

        val result = tableEnv.sqlQuery("select dt, AverageTemperature from source " +
                "where Country = 'United States'")

        val collectionSink = CollectionSink<Row>()
        collectionSink.clearStuffs()
        tableEnv.toAppendStream(result, Row::class.java).addSink(collectionSink)

//        Deprecated, see above code using addSink() instead
//        val csvSink = CsvTableSink(testOutPath, ",")
//        result.writeToSink(csvSink)

        val execResult = env.execute()

        assertThat(collectionSink.getStuffs()).hasSize(2941)
    }


    fun buildGlobalTempCsvTable(tableEnv: TableEnvironment, sourcePath: String = GLOBAL_TEMPERATURE_FILE): CsvTableSource {
        val csvTable = CsvTableSource.builder()
                .path(sourcePath)
                .ignoreFirstLine()
                .fieldDelimiter(",")
                .field("dt", Types.SQL_DATE())
                .field("AverageTemperature", Types.DOUBLE())
                .field("AverageTemperatureUncertainty", Types.DOUBLE())
                .field("Country", Types.STRING())
                .build()

        tableEnv.registerTableSource("source", csvTable)

        return csvTable
    }
}