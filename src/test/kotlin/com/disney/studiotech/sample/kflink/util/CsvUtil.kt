package com.disney.studiotech.sample.kflink.util

import com.opencsv.CSVReader
import java.io.BufferedReader
import java.io.FileReader

object CsvUtil {

    fun readAll(filename: String ) : List< Array<String?> > {
        var records = arrayListOf< Array<String?> >()

        val reader = BufferedReader(FileReader(filename))
        val csvReader = CSVReader(reader)

        // Skip header
        csvReader.readNext()

        var record: Array<String>? = csvReader.readNext()
        while (record != null) {
            record = csvReader.readNext()
            records.add (record)
        }
        reader.close()

        return records
    }
}