package com.disney.studiotech.sample.kflink.model

data class Movie (
    val directorName: String? = null,
    val duration: Long? = null,
    val title: String? = null,
    val language: String? = null,
    val country: String? = null,
    val actor1Name: String? = null,
    val actor2Name: String? = null,
    val gross: Long? = null
    ){}