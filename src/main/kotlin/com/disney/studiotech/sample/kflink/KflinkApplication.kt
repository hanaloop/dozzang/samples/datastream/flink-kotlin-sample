package com.disney.studiotech.sample.kflink

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KflinkApplication

fun main(args: Array<String>) {
    runApplication<KflinkApplication>(*args)
}

