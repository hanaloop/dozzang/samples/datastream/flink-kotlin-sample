package com.disney.studiotech.sample.kflink.util

import org.apache.flink.streaming.api.functions.sink.SinkFunction

/**
 * A sink that collects the output into a collection.
 * Used for test purpose. It would not work if the result do not fit in the memory.
 */
class CollectionSink<IN>: SinkFunction<IN> {

    companion object {
        // This must be static or won't be populated.
        var stuffs = arrayListOf<Any>()
    }

    fun clearStuffs() {
        stuffs.clear()
    }
    fun getStuffs(): List<Any> {
        return stuffs
    }

    @Throws(Exception::class)
    override
    fun invoke(value: IN, context: SinkFunction.Context<*>) {
        stuffs.add(value as Any)
    }
}