Spring Spark Sample
===================

This is a sample application that exemplifies using Kotlin and Flink to 
execute queries on a csv files using Flink SQL.

NOTE: There is nothing interesting in the src folder, instead
look the samples at the test code
```
src/test/kotlin/com/disney/studiotech/sample/kflink/KFlinkTest.kt
```

# Library stack
- Spring Boot v2
- Spring Boot Starter Web 
- Kotlin 1.2
- Flink 1.7
- Flink Streaming and Table

# Testing and running the application
## Run the tests
```
mvn verify
```


### Data
This project uses Global temperature data from  data.wold
```
testdata/GlobalLandTemperatures_GlobalLandTemperaturesByCountry.csv
```


# References
- [Global temperature](https://data.world/data-society/global-climate-change-data)
- [Movies data](https://data.world/popculture/imdb-5000-movie-dataset)
